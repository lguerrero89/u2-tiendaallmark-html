
var startCountDown = function( date, content ){
    // Set the date we're counting down to
    var countDownDate = new Date(date).getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

      // Get todays date and time
      var now = new Date().getTime();

      // Find the distance between now an the count down date
      var distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
      if( seconds<10 ){ seconds = '0'+seconds; }

      // Display the result in the element with id="demo"
      jQuery( '.row'+content+'time' ).html( '<div class="cell">'+ hours + '</div><div class="cell">' + minutes + '</div><div class="cell">' + seconds + '</div>' );


      // If the count down is finished, write some text
      if (distance < 0) {
        clearInterval(x);
        jQuery( content ).html( "OFERTA EXPIRADA" );
      }
    }, 1000);

},showAlert = function (msg) {
  $('#alertModal .modal-body').html( msg );
  $('#alertModal').modal();
}, showAlertLarge = function(msg){
  jQuery('#largeModal .modal-body').html( msg );
  jQuery('#largeModal').modal();
};



jQuery(window).on("load", function() {

    //Load CountDown Products
    startCountDown('May 11, 2017 15:37:25', '.countdown');

    if( jQuery('.sidebar-right').size()>0 ){
        jQuery('.show-sidebar').on('click', function(e){
            e.preventDefault();
            if( jQuery('.sidebar-right').hasClass('active') ){
                $( ".sidebar-right" ).animate({
                    right: "-250px"
                }, 500, function() {
                    jQuery('.sidebar-right').removeClass('active')
                });
            } else {
                $( ".sidebar-right" ).animate({
                    right: "0"
                }, 500, function() {
                    jQuery('.sidebar-right').addClass('active')
                });
            }
        });
    }

    // jQuery('button.iframe').fancybox();
    setTimeout(function(){
        //showAlertLarge('Hola mundo');
        //$("a#single_image").fancybox();
    }, 2000);


});